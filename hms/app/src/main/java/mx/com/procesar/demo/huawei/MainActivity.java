package mx.com.procesar.demo.huawei;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import java.util.Map;

import mx.com.procesar.demo.huawei.crashlytics.CrashlyticsActivity;
import mx.com.procesar.demo.huawei.maps.MapsActivity;
import mx.com.procesar.demo.huawei.metrics.MetricsActivity;
import mx.com.procesar.demo.huawei.push.PushActivity;

public class MainActivity extends AppCompatActivity  {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatButton btnViewMap = findViewById(R.id.btn_view_maps);
        AppCompatButton btnViewCrash = findViewById(R.id.btn_view_crash);
        AppCompatButton btnViewPush = findViewById(R.id.btn_view_push);
        AppCompatButton btnViewMetrics = findViewById(R.id.btn_view_metrics);

        btnViewMap.setOnClickListener(mOnClickListener);
        btnViewCrash.setOnClickListener(mOnClickListener);
        btnViewPush.setOnClickListener(mOnClickListener);
        btnViewMetrics.setOnClickListener(mOnClickListener);

        isLaunchedFromDeepLink();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Uri data = intent.getData();
        Bundle mBundle = intent.getExtras();
        Log.i(TAG, "onNewIntent " + "\n" +
                "data :" + data + "\n" +
                "bundle :" + mBundle
        );

        if (data != null || mBundle != null){
            Log.i(TAG, "isLaunchedFromDeepLink " + "\n" +
                    "NOTIFICATION_ID :" + mBundle.getInt("NOTIFICATION_ID") + "\n" +
                    "DEEP_LINK_URL :" + mBundle.getString("DEEP_LINK_URL") + "\n" +
                    "DEEP_LINK_METHOD :" + mBundle.getString("DEEP_LINK_METHOD") + "\n" +
                    "DEEP_LINK_NAV_KEY :" + mBundle.getString("DEEP_LINK_NAV_KEY") + "\n" +
                    "DEEP_LINK_KEY :" + mBundle.getString("DEEP_LINK_KEY") + "\n" +
                    "DEEP_LINK_ID_NOTIFICATION :" + mBundle.getString("DEEP_LINK_ID_NOTIFICATION") + "\n" +
                    "DEEP_LINK_SUBKEY :" + mBundle.getString("DEEP_LINK_SUBKEY") + "\n"
            );
        }
    }

    private final View.OnClickListener mOnClickListener = v -> {
        switch (v.getId()){
            case R.id.btn_view_maps:
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
                break;
            case R.id.btn_view_crash:
                startActivity(new Intent(MainActivity.this, CrashlyticsActivity.class));
                break;
            case R.id.btn_view_push:
                startActivity(new Intent(MainActivity.this, PushActivity.class));
                break;
            case R.id.btn_view_metrics:
                startActivity(new Intent(MainActivity.this, MetricsActivity.class));
                break;
            default:
                break;
        }
    };

    private boolean isLaunchedFromDeepLink() {
        Uri data = getIntent().getData();
        Bundle mBundle = getIntent().getExtras();
        Log.i(TAG, "isLaunchedFromDeepLink " + "\n" +
                "data :" + data + "\n" +
                "bundle :" + mBundle
        );

        if (data != null || mBundle != null){
            Log.i(TAG, "isLaunchedFromDeepLink " + "\n" +
                    "NOTIFICATION_ID :" + mBundle.getInt("NOTIFICATION_ID") + "\n" +
                    "DEEP_LINK_URL :" + mBundle.getString("DEEP_LINK_URL") + "\n" +
                    "DEEP_LINK_METHOD :" + mBundle.getString("DEEP_LINK_METHOD") + "\n" +
                    "DEEP_LINK_NAV_KEY :" + mBundle.getString("DEEP_LINK_NAV_KEY") + "\n" +
                    "DEEP_LINK_KEY :" + mBundle.getString("DEEP_LINK_KEY") + "\n" +
                    "DEEP_LINK_ID_NOTIFICATION :" + mBundle.getString("DEEP_LINK_ID_NOTIFICATION") + "\n" +
                    "DEEP_LINK_SUBKEY :" + mBundle.getString("DEEP_LINK_SUBKEY") + "\n"
            );
            return true;
        }

        return false;
    }

}