package mx.com.procesar.demo.huawei.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {

    private final SharedPreferences preferences;

    private static final String PREFERENCES_NAME = "my.preferences";

    public static final String TOKEN_HUAWEI = "TOKEN_HUAWEI";


    public PreferencesHelper(Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    //CONTAIN
    public boolean contains(String keyPreference){
        return preferences.contains(keyPreference);
    }


    //SAVE
    public void saveBoolean(String keyPreference, boolean value) {
        preferences.edit()
                .putBoolean(keyPreference, value)
                .commit();
    }

    public void saveInt(String keyPreference, int value) {
        preferences.edit()
                .putInt(keyPreference, value)
                .commit();
    }

    public void saveString(String keyPreference, String value) {
        preferences.edit()
                .putString(keyPreference, value)
                .commit();
    }

    //GET
    public boolean getBoolean(String keyPreference) {
        return preferences.getBoolean(keyPreference, false);
    }

    public int getInt(String keyPreference) {
        return preferences.getInt(keyPreference, 0);
    }

    public String getString(String keyPreference) {
        return preferences.getString(keyPreference, "");
    }

    //CLEAR
    public boolean clear(String keyPreference){
        return preferences.edit().remove(keyPreference).commit();
    }
}
