package mx.com.procesar.demo.huawei.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.huawei.hms.push.HmsMessageService;
import com.huawei.hms.push.RemoteMessage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import mx.com.procesar.demo.huawei.MainActivity;
import mx.com.procesar.demo.huawei.R;

public class HuaweiMessagingService extends HmsMessageService  {
    private static final String TAG = "HuaweiMessagingService";

    public static final String PUSH_METHOD = "method";
    public static final String PUSH_KEY = "uKey";
    public static final String PUSH_NAV_KEY = "navKey";
    public static final String PUSH_URL = "url";
    public static final String PUSH_ID_NOTIFICATION = "idNotificacion";
    public static final String PUSH_SUBKEY = "subNavKey";
    public static final String ANDROID_CHANNEL_ID = "AforeMovil";
    public static final String ANDROID_CHANNEL_NAME = "AM_ANDROID";

    NotificationManager mNotifyMgr;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.i(TAG, "receive token:" + token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        int mNotificationId = remoteMessage.getNotification().hashCode();
        Map<String, String> data = remoteMessage.getDataOfMap();

        String method = data.get(PUSH_METHOD);
        String navkey = data.get(PUSH_NAV_KEY);
        String ukey = data.get(PUSH_KEY);
        String url = data.get(PUSH_URL);
        String idNotification = data.get(PUSH_ID_NOTIFICATION);
        String subkey = data.get(PUSH_SUBKEY);

        Log.i(TAG, "Datos  -->  \n" +
                "getData  --> " + remoteMessage.getData() + "\n" +
                "getDataOfMap  --> " + remoteMessage.getDataOfMap() + "\n" +
                "method  --> " + method + "\n" +
                "ukey  --> " + ukey + "\n" +
                "navkey  --> " + navkey + "\n" +
                "url  --> " + url + "\n" +
                "idNotification  --> " + idNotification + "\n" +
                "subkey  --> " + subkey + "\n"
        );

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent;

        Intent intent = new Intent(this, MainActivity.class);

        intent.putExtra("NOTIFICATION_ID", mNotificationId);
        intent.putExtra("DEEP_LINK_URL", url);
        intent.putExtra("DEEP_LINK_METHOD", method);
        intent.putExtra("DEEP_LINK_NAV_KEY", navkey);
        intent.putExtra("DEEP_LINK_KEY", ukey);
        intent.putExtra("DEEP_LINK_ID_NOTIFICATION", idNotification);
        intent.putExtra("DEEP_LINK_SUBKEY", subkey);

        pendingIntent = PendingIntent.getActivity(this, requestID, intent,
                PendingIntent.FLAG_ONE_SHOT);

        createNotification(remoteMessage, pendingIntent, uri, mNotificationId);
    }

    private void createNotification(RemoteMessage remoteMessage, PendingIntent pendingIntent, Uri uri, int mNotificationId) {
        String title = getString(R.string.app_name);

        if (remoteMessage.getNotification().getTitle() != null) {
            title = remoteMessage.getNotification().getTitle();
        }

        mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, ANDROID_CHANNEL_ID)
                        .setLargeIcon(
                                BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle(title)
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setSound(uri);

        Notification notificationCompat = mBuilder.build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel androidChannel = new NotificationChannel(ANDROID_CHANNEL_ID,
                    ANDROID_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            androidChannel.enableLights(true);
            androidChannel.enableVibration(true);
            androidChannel.setLightColor(Color.GREEN);
            androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (mNotifyMgr != null) {
                mNotifyMgr.createNotificationChannel(androidChannel);
            }
        }

        if (mNotifyMgr != null) {
            mNotifyMgr.notify(mNotificationId, notificationCompat);
        }

    }
}
