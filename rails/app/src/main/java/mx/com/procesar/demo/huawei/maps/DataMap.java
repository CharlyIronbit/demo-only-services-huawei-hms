package mx.com.procesar.demo.huawei.maps;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataMap {

    public static Map<String, List<Double>> markets() {
        HashMap<String, List<Double>> map = new HashMap<>();
        map.put("Un punto de mira gigante", Arrays.asList(37.563936,-116.85123));
        map.put("Procesar", Arrays.asList(19.4002529, -99.2270576));
        map.put("CDMX", Arrays.asList(19.3910038, 99.2836993));
        map.put("Japon", Arrays.asList(32.6943697, 129.4193845));
        map.put("Londres", Arrays.asList(51.5287718, 0.2416818));
        map.put("Paris", Arrays.asList(48.8589507, 2.2770201));
        map.put("Tlaxcala", Arrays.asList(19.4174275, 98.4471142));
        map.put("España", Arrays.asList(40.1300892, 8.2036237));
        map.put("Chapultepec", Arrays.asList(19.4166864, 99.1927007));
        map.put("Tlapan", Arrays.asList(19.2009697, 99.2786299));
        map.put("Xochimilco", Arrays.asList(19.2366455, 99.1507603));
        return map;
    }

}
